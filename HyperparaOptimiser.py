
from sklearn.ensemble import RandomForestRegressor
from sklearn import datasets
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score
import pdb as pdb
from operator import itemgetter
import matplotlib.pyplot as plt

#for parallel processing
import multiprocessing as mp
from functools import partial

class HyperparaOptimiser:
    '''This class is used to select the optimal hyperparameters for a ML model.
    There are four methods generateParameterModels, optimiseParameters, predictVals and getData
    getData- get the data in X and Y format
    predictVals - uses the model to predict values and returns a r2 score
    optimiseParameters - Loops through all parameters combination
    generateParameterModels- Generate a complete list of models with all parameter choice - TO CHANGE THIS TO ADD NEW MODELS + PARAMETERS
    An example of the class utility on a dataset is in the __main__ section '''

    def __init__(self, ml_model, parameters, paraSpace):

        self.ml_model = ml_model # ML model
        self.parameters = parameters
        self.paraSpace = paraSpace
        #self.generateParameterModels() # generates combination of all models

    def generateParameterModels(self):
        '''This method allocates the combinations of ML models with the various parameters.
         - If ML model and parameters needs to be added or deleted, this needs to be updated'''
        paraSpace = self.paraSpace

        #to add here if more parameters need to added or deleted for optimisation
        n_estimators, max_depth, criterion = itemgetter('n_estimators', 'max_depth', 'criterion')(paraSpace)

        param_dist = [] #generate all combinations
        for n_est in n_estimators:
            for m_d in max_depth:
                for c in criterion:
                    param_dist.append({"n_estimators": n_est,
                                       "max_depth": m_d,
                                       "criterion": c})

        # Generate ML models with parameters - Add here if new model is to be tested
        #models = [RandomForestRegressor(**i) for i in param_dist]
        if self.ml_model=='randomForestRegressor':
            self.models = [RandomForestRegressor(**i) for i in param_dist]


    def optimiseParameters(self, thresh_r2val, useParallel=False):
        ''''This method optimises the parameters using a halving method, where small amount of data are trained on all 
        parameter spaces, and are halved and run with a larger data incrementally, till the  error metric (r2) is
         satisfied or the last parameter set is left'''

        self.generateParameterModels()

        # Ratio of data splitting - (1-x is used as using the train test func of sklearn)
        splitDataRatios = 1 - np.array([0.2, 0.4, 0.8, 0.99])

        # Data for model tuning - in future if data is too large to load in memory to replace splitting data with data loading
        X = self.X
        Y = self.Y
        models = self.models
        nprocs = mp.cpu_count()

        #Main loop for halving the parameters
        trackMaxR2Val=[0] #var to check if the optimisation is improving or not
        for num, ratio in enumerate(splitDataRatios):
            [x_subSet, x_testSS, y_subSet, y_testSS] = train_test_split(X, Y, test_size=ratio,
                                                                        random_state=0)  #Split data
            print(f"{num+1} iteration: The no of models to be tested are {len(models)}")
            print(f"{num+1} iteration: The length of data used is {len(x_subSet)}")

            r2Vals = np.zeros(len(models))

            #Loop through all models and get r2 error metric - use normal or multiprocessing
            if useParallel ==True:
                # running in parallel
                func = partial(self.predictVals, x_subSet, y_subSet)
                with mp.Pool(processes=nprocs-1) as pool:  # can tune no of processors spawned. set as max - 1 for now
                    r2Vals = pool.map(func, models)
                r2Vals=np.array(r2Vals)
            else: # single core
                for i, modelSel in enumerate(models):
                    r2Vals[i] = self.predictVals(x_subSet, y_subSet, modelSel)


            #Plots to see what the errors are like
            #plt.plot(r2Vals)
            #plt.show()
            #pdb.set_trace()

            trackMaxR2Val.append(np.mean(r2Vals)) #using average of R2 to see if the R2 values change
            avgValError = np.median(trackMaxR2Val)
            print(f"Average R2 over all models is {avgValError}")
            #Check if model accuracy has been reached or else remove lower half of parameters
            if np.max(r2Vals)>thresh_r2val or np.max(r2Vals)<=avgValError:
                pdb.set_trace()
                break
            elif ratio!=splitDataRatios[-1]:  #also no need to do this for last iteration as you want to find the optimal model
                # sort and remove half of parameters
                sort_index = np.argsort(r2Vals)  # sorts lowest to highest - index values
                valuesToRemove = round(len(sort_index) / 2) # remove half the values
                #remove parameters
                for ele in sorted(sort_index[0:valuesToRemove], reverse=True):
                    del models[ele]

        #Save and output optimal model set
        #pdb.set_trace()
        indxMax =r2Vals.argmax()
        self.optimalModel = models[indxMax]
        self.optimalModelR2Val = np.max(r2Vals)


    def predictVals(self,xData, yData,model):
        ''' Testing and prediction of ML model
        :input: model: is the ML model used
        :input: xData and yData are the data used for training and testing
        :return: r2 score for effectiveness of model based on testing
        '''
        try:
            # splitting data into 30% for testing - to do k fold validation later
            [x_train, x_test, y_train, y_test] = train_test_split(xData, yData, test_size=0.3)

            # Fit model and predict test data
            # pdb.set_trace()
            # model.fit(x_train,y_train)
            model.fit(xData, yData)
            y_pred = model.predict(x_test)

            # Compute error metric
            r2Val = r2_score(y_test, y_pred)
            mse = mean_squared_error(y_test, y_pred)
            rmse = np.sqrt(mse)

        except:
            #print("Parameter set not used")  # to setup to a logging function later
            r2Val = 0

        return r2Val


    def getData(self,dataLoc):
        #at present using the sklean boston dataset - to modify accordingly based on other data formats
        if dataLoc=='boston_dataset':
            boston = datasets.load_boston()
            self.X = boston.data
            self.Y = boston.target


if __name__ == '__main__':

    #initialise class
    A= HyperparaOptimiser(ml_model='randomForestRegressor',
                       parameters =['n_estimators','max_depth','criterion'],
                       paraSpace={'n_estimators':np.random.randint(low=10, high=100, size=20),
                                  'max_depth':np.random.randint(low=3, high=5, size=5),
                                  'criterion':["mse","gini"]})

    # Assign dataset
    A.getData('boston_dataset')

    #Optimise parameters
    r2Max=0.99 #Defined accuray required model fit (R2 coeffecient used- range 0-1)
    useParallel=True # True/Flase flag as to use parallel processing
    A.optimiseParameters(r2Max,useParallel)

    print(f"The optimal model is {A.optimalModel}")
    print(f"and its R2 coeffecient is {A.optimalModelR2Val}")
    #pdb.set_trace()