# -*- coding: utf-8 -*-
"""
@author: Nira
Scripting hyperoptimisation and testing functionality
"""

import pandas as pd
import numpy as np
import pdb as pdb

import matplotlib.pyplot as plt

import time

from sklearn import datasets
from sklearn import model_selection
from sklearn.ensemble import RandomForestRegressor
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score

import multiprocessing as mp
nprocs = mp.cpu_count()
#pool = mp.Pool(processes=nprocs)
print(f"Number of CPU cores: {nprocs}")

# loading and splitting data
boston = datasets.load_boston()

X = boston.data
Y = boston.target

# Splitting dataset
[x_train, x_test, y_train, y_test] = train_test_split(X, Y, test_size=0.9,
                                                      random_state=0)  # test size to change on each loop

#Testing ML model
rf_Mdl = RandomForestRegressor()
rf_Mdl.fit(x_train, y_train)

# Doing a random grid optimisation

# Parameters for testing

# from scipy.stats import expon as sp_expon
# from scipy.stats import randint as sp_randint
# n_estimators = sp_expon(scale=100)
# max_depth = sp_randint(1, 40)

# Parameters to optimise
n_estimators = np.random.randint(low=10, high=100, size=15)
max_depth = np.random.randint(low=3, high=5, size=15)
criterion = ["mse"]

# generate list of dictionary for all available parameters [#g1]
param_dist = []
for n_est in n_estimators:
    for m_d in max_depth:
        for c in criterion:
            param_dist.append({"n_estimators": n_est,
                               "max_depth": m_d,
                               "criterion": c})

# generate RF models with parameters [#g2]
models = [RandomForestRegressor(**i) for i in param_dist]
# g1 and g2 can be combined later

# function to predict values
def predictVals(model, xData, yData):

    try:
        # splitting data into 30% for testing - to do k fold validation later 
        [x_train, x_test, y_train, y_test] = train_test_split(xData, yData, test_size=0.3)

        # Fit model and predict test data
        # pdb.set_trace()
        # model.fit(x_train,y_train)
        model.fit(xData, yData)
        y_pred = model.predict(x_test)

        # Compute error metric
        mse = mean_squared_error(y_test, y_pred)
        rmse = np.sqrt(mse)
        r2Val = r2_score(y_test, y_pred)
    except:
        print("Parameter set not used") # to set to a logging function later
        r2Val = 0

    return r2Val

# Use a half splitting approach to optimisation, and grow data inputted at each iteration, while trimming parameters
splitDataRatios = [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1]

for ratio in splitDataRatios:
    [x_subSet, x_testSS, y_subSet, y_testSS] = train_test_split(X, Y, test_size=ratio,
                                                                random_state=0)  # test size to change on each loop
    print(f"The no of models to be tested are {len(models)}")
    print(f"The length of data used is {len(x_subSet)}")
    r2Vals = np.zeros(len(models))

    #r2Vals=pool.starmap(predictVals, [(modelSel,x_subSet,y_subSet) for modelSel in models])
    #pdb.set_trace()

    for i, modelSel in enumerate(models):
        r2Vals[i] = predictVals(modelSel, x_subSet, y_subSet)

    plt.plot(r2Vals)
    plt.show()
    #pdb.set_trace()

    # sort values
    sort_index = np.argsort(r2Vals) #sorts lowest to highest - index values

    valuesToRemove = round(len(sort_index)/2)

    for ele in sorted(sort_index[0:valuesToRemove], reverse=True):
        del models[ele]


"""
for ratio in splitDataRatios:
    [x_subSet, x_testSS, y_subSet, y_testSS] = train_test_split(X, Y, test_size=ratio,
                                                                random_state=0)  # test size to change on each loop

    r2Vals = np.zeros(len(models))
    for i, modelSel in enumerate(models):
        # pdb.set_trace()
        try:
            r2Vals[i] = predictVals(modelSel, x_subSet, y_subSet)
            # pdb.set_trace()
        except:
            # pdb.set_trace()
            print('Skipping a parameter set as it is not suitable')

    # sort values
    yNew = r2Vals.sort(axis=0)
    indx = r2Vals.argsort(axis=0)
    pdb.set_trace()
"""