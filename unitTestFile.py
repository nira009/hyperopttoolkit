import unittest
import numpy as np

import HyperparaOptimiser as hp

class MyTestCase(unittest.TestCase):

    def test_r2Val(self):
        '''
        Unit test for r2 value of model fit. Checking if above 0.8. In future to check with Aroc curves and pvalue.
        '''
        A = hp.HyperparaOptimiser(ml_model='randomForestRegressor',
                               parameters=['n_estimators', 'max_depth', 'criterion'],
                               paraSpace={'n_estimators': np.random.randint(low=10, high=100, size=15),
                                          'max_depth': np.random.randint(low=3, high=5, size=5),
                                          'criterion': ["mse"]})
        A.getData('boston_dataset')
        r2Max = 0.99
        useParallel = True
        A.optimiseParameters(r2Max, useParallel)  # only used ony error metric, in future can add other metrics
        self.assertGreaterEqual(A.optimalModelR2Val,0.8) #check if model is great - anticiapted that >0.8 usinf Random Forest


if __name__ == '__main__':
    unittest.main()
